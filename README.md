# THEOplayer iOS Reference Apps - THEO Offline

The purpose of this iOS project is to demonstrate how to integrate THEOplayer into an iOS app and playback a HLS stream.


## Getting Started

### Prerequisites

* Cloned **_THEO Offline Reference App_**.
* XCode 11.1 pre-installed. 
* Obtained **_THEOplayer iOS SDK Framework_**. If you don't have a SDK yet, please visit [Get Started with THEOplayer].

### Open Simple Playback Project
1. Launch **_Xcode_** from **_LaunchPad_** and click on `Open another project...`

2. Navigate to the downloaded project, select `theo_offline.xcodeproj` and click `Open` theo_offline.xcodeproj

### Import THEOplayer SDK Framework
1. Drag and drop **_THEOplayer iOS SDK Framework_** file from **_Finder_** 

2. In the option popup screen, select options as below and click `Finish`

3. Embed **_THEOplayer iOS SDK Framework_** to the project

### Build and run the app
1. Select the desire device / simulator and run the app





