//
//  OfflineViewController.swift
//  theo_offline
//
//  Copyright © 2019 THEOPlayer. All rights reserved.
//

import UIKit

// MARK: - OfflineViewController declaration

class OfflineViewController: UIViewController {

    // MARK: - View properties

    private var tableView: UITableView!
    private var navigationBarTitle = ""

    // ViewModel object that contains the streams
    private let offlineViewViewModel: OfflineViewViewModel = OfflineViewViewModel()

    // MARK: - View controller life cycle

    override func viewDidLoad() {
        setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

        // NavifationItem title will set to empty string before pushing PlayerViewController, hence need to reset it here.
        navigationItem.title = navigationBarTitle
    }

    // MARK: - View setup

    private func setupUI() {
        setupView()
        setupTableView()
    }

    private func setupView() {
        navigationBarTitle = navigationController?.navigationBar.topItem?.title ?? ""

        view.backgroundColor = .theoCello
    }

    private func setupTableView() {
        tableView = THEOComponent.tableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(OfflineTableViewCell.self, forCellReuseIdentifier: "offlineStreamCell")
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension

        view.addSubview(tableView)

        // Padding with constraints around the screen edges
        tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -10).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 10).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -10).isActive = true
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource

extension OfflineViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offlineViewViewModel.cellViewModels.count
    }

    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // Reset viewModel delegate to avoid unexpected UI changes to the offlineStreamCell
        if let offlineStreamCell = cell as? OfflineTableViewCell {
            offlineStreamCell.viewModel?.delegate = nil
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // DequeueReusableCell for OfflineTableView as OfflineTableViewCell
        let cell = tableView.dequeueReusableCell(withIdentifier: "offlineStreamCell", for: indexPath)
        if let offlineStreamCell = cell as? OfflineTableViewCell {
            offlineStreamCell.delegate = self
            // Assign viewModel to the cell object and set the cell as the viewModel's delegate
            offlineStreamCell.viewModel = offlineViewViewModel.cellViewModels[indexPath.row]
            offlineStreamCell.viewModel?.delegate = offlineStreamCell
        }

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Instantiate playerViewController and assign playerViewModel
        if let offlineStreamCell = tableView.cellForRow(at: indexPath) as? OfflineTableViewCell {
            let playerViewController = PlayerViewController()
            playerViewController.viewModel = PlayerViewViewModel(source: offlineStreamCell.viewModel!.sourceDescription)
            navigationItem.title = ""
            navigationController?.pushViewController(playerViewController, animated: true)
        }
    }
}

// MARK: - OfflineTableViewCellDelegate

extension OfflineViewController: OfflineTableViewCellDelegate {
    func onPresent(alertController: UIAlertController) {
        present(alertController, animated: true, completion: nil)
    }
}
