//
//  OfflineTableViewCellViewModel.swift
//  theo_offline
//
//  Copyright © 2019 THEOPlayer. All rights reserved.
//

import os.log
import THEOplayerSDK

// MARK: - OfflineTableViewCellViewModelDelegate declaration

protocol OfflineTableViewCellViewModelDelegate {
    func onProgressUpdate(precentage: Double)
    func onCacheResumed()
    func onCacheCompleted()
    func onCacheRemoved()
    func onError()
}

// MARK: - OfflineTableViewCellViewModel declaration

class OfflineTableViewCellViewModel {

    // MARK: - Private property

    private let expiryInMinutes: Int = 120

    // MARK: - Public properties

    var title: String = ""
    var posterImage: UIImage?
    var url: String = ""
    var mimeType: String = ""
    var sourceDescription: SourceDescription!
    var cachingTask: CachingTask? = nil {
        didSet {
            if let task = cachingTask {
                attachCachingEventListeners()
                switch task.status {
                case .idle:
                    // No action for newly created caching task
                    break
                case .loading:
                    delegate?.onCacheResumed()
                case .error:
                    delegate?.onError()
                case .done:
                    delegate?.onCacheCompleted()
                case .evicted:
                    // Should never happen as evicated cachingTask will be not set in OfflineViewViewModel
                    break
                @unknown default:
                    break
                }
            } else {
                removeCachingEventListeners()
            }
        }
    }
    var taskPrecentage: Double {
        get {
            if let task = cachingTask, !task.percentageCached.isNaN {
                return task.percentageCached
            } else {
                return 0.0
            }
        }
    }
    var isCached: Bool {
        get {
            return cachingTask?.status ?? .idle == .done
        }
    }
    var isEvicted: Bool {
        get {
            return cachingTask?.status ?? .idle == .evicted
        }
    }
    var cachingListener: [String : EventListener] = [:]
    var delegate: OfflineTableViewCellViewModelDelegate? = nil

    // MARK: - Class life cycle

    init(stream: Stream) {
        title = stream.title
        posterImage = UIImage(named: stream.posterName)
        url = stream.url
        mimeType = stream.mimeType
        sourceDescription = SourceDescription(
            source: TypedSource(src: url, type: mimeType),
            poster: stream.posterUrl
        )
    }

    deinit {
        // Set caching task to nil will also remove event listener
        cachingTask = nil
    }

    // MARK: - Cache event listener related functions and closures

    private func attachCachingEventListeners() {
        // Listen to caching event and store references in dictionary
        cachingListener["stateChange"] = cachingTask?.addEventListener(type: CachingTaskEventTypes.STATE_CHANGE, listener: onStateChangeEvent)
        cachingListener["progress"] = cachingTask?.addEventListener(type: CachingTaskEventTypes.PROGRESS, listener: onProgressEvent)
    }

    private func removeCachingEventListeners() {
        // Remove caching event listenrs
        cachingTask?.removeEventListener(type: CachingTaskEventTypes.STATE_CHANGE, listener: cachingListener["stateChange"]!)
        cachingTask?.removeEventListener(type: CachingTaskEventTypes.PROGRESS, listener: cachingListener["progress"]!)

        cachingListener.removeAll()
    }

    private func onStateChangeEvent(event: CacheEvent) {
        os_log("onStateChangeEvent status: %@", self.cachingTask?.status.rawValue ?? "")
        if let status = cachingTask?.status {
            switch status {
            case .done:
                delegate?.onCacheCompleted()
            case .error:
                delegate?.onError()
            case .evicted:
                // Currently THEO SDK is not firing evicted event from the main thread hence the dispatch to main queue block below
                DispatchQueue.main.async {
                    self.delegate?.onCacheRemoved()
                }
            default:
                // This covers .idle and .loading cases where no action is needed
                break
            }
        }
    }

    private func onProgressEvent(event: CacheEvent) {
        if let task = cachingTask,
            let precentage = cachingTask?.percentageCached,
            !precentage.isNaN {
            os_log("title: %@, status: %@, percentage: %.2f", title,  task.status.rawValue, task.percentageCached * 100)
            delegate?.onProgressUpdate(precentage: precentage)
        }
    }

    // MARK: - Caching task functions

    func createCachingTask() {
        let target = Calendar.current.date(byAdding: .minute, value: expiryInMinutes, to: Date())
        cachingTask = THEOplayer.cache.createTask(source: sourceDescription, parameters: CachingParameters.init(expirationDate: target!))
        cachingTask?.start()
        os_log("createCachingTask: status : %@ bytesCached: %d", cachingTask?.status.rawValue ?? "nil", cachingTask?.bytesCached ?? 0)
    }

    func pauseCaching() {
        cachingTask?.pause()
        os_log("pauseCaching: status : %@ bytesCached: %d", cachingTask?.status.rawValue ?? "nil", cachingTask?.bytesCached ?? 0)
    }

    func resumeCaching() {
        cachingTask?.start()
        os_log("resumeCaching: status : %@ bytesCached: %d", cachingTask?.status.rawValue ?? "nil", cachingTask?.bytesCached ?? 0)
    }

    func removeCaching() {
        os_log("removeCaching: status : %@ bytesCached: %d", cachingTask?.status.rawValue ?? "nil", cachingTask?.bytesCached ?? 0)
        cachingTask?.remove()
        cachingTask = nil
    }
}
