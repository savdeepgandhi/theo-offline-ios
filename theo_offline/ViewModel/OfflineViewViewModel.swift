//
//  OfflineViewViewModel.swift
//  theo_offline
//
//  Copyright © 2019 THEOPlayer. All rights reserved.
//

import os.log
import THEOplayerSDK

// MARK: - OfflineViewViewModel declaration

class OfflineViewViewModel {

    // MARK: - Private property

    // Static Stream array declaration used in this app
    private let streams: [Stream] = [
        Stream(title: "Big Buck Bunny",
               posterName: "big-buck-bunny",
               posterUrl: "https://cdn.theoplayer.com/video/big_buck_bunny/poster.jpg",
               url: "https://cdn.theoplayer.com/video/big_buck_bunny/big_buck_bunny_metadata.m3u8",
               mimeType: "application/x-mpegURL"),
        Stream(title: "Sintel",
               posterName: "sintel",
               posterUrl: "https://cdn.theoplayer.com/video/sintel/poster.jpg",
               url: "https://cdn.theoplayer.com/video/sintel/nosubs.m3u8",
               mimeType: "application/x-mpegURL"),
        Stream(title: "Tears of Steel",
               posterName: "tears-of-steel",
               posterUrl: "https://cdn.theoplayer.com/video/tears_of_steel/poster.jpg",
               url: "https://cdn.theoplayer.com/video/tears_of_steel/index.m3u8",
               mimeType: "application/x-mpegURL"),
        Stream(title: "Elephants Dream",
               posterName: "elephants-dream",
               posterUrl: "https://cdn.theoplayer.com/video/elephants-dream/playlist.png",
               url: "https://cdn.theoplayer.com/video/elephants-dream/playlist.m3u8",
               mimeType: "application/x-mpegURL")
    ]

    // MARK: - Public property

    var cellViewModels: [OfflineTableViewCellViewModel] = [OfflineTableViewCellViewModel]()

    // MARK: - Class life cycle

    init() {
        // Loop through the streams array and instantiate OfflineTableViewCellViewModel objects
        for stream in streams {
            let offlineTableViewCellViewModel = OfflineTableViewCellViewModel(stream: stream)

            /* Check status of all existing caching tasks
                If task status is evicted, remove the task
                Else assign the task to the view model object
             */
            for task in THEOplayer.cache.tasks {
                for source in task.source.sources {
                    if source.src == URL(string: stream.url) {
                        os_log("Found caching task for URL: %@, task status: %@", stream.url, task.status.rawValue)
                        switch task.status {
                        case .idle, .done, .loading, .error:
                            offlineTableViewCellViewModel.cachingTask = task
                            continue
                        case .evicted:
                            task.remove()
                        @unknown default:
                            continue
                        }
                    }
                }
            }

            cellViewModels.append(offlineTableViewCellViewModel)
        }
    }
}
