//
//  Stream.swift
//  theo_offline
//
//  Copyright © 2019 THEOPlayer. All rights reserved.
//

// MARK: - Stream declaration

struct Stream {
    var title: String
    var posterName: String
    var posterUrl: String
    var url: String
    var mimeType: String
}
